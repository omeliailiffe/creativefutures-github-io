---
title: "How To Use Git"
description: ""
hidden: true
permalink: /git/
---

git fetch (download any changes)
git status (ahead/behind)
if behind = git pull (download)
if ahead = git push (upload)
to add files = git add (-A / add all files)
commit = git commit (-m "" / add description)
