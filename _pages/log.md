---
layout: onepage
title: "Thinking Log"
description: "A log of our thinking"
---

### Initial Thinking
**We don't want:**
 * a boring project
 * an environmentally focussed project (but an environmentally conscious)
 * non people connecting focussed

**We want:**
 * an Open Source project
 * allowing people to take what we learn, remix it and make it better.
 * to document our project on Github
 * our project to exist in the techspace (with analog + digital tech)
 * to be accessible in all aspects


 **Ultimaker as an example:**
 * Open source everything
 * We can make it best for you
 * Or you can make it yourself


**Educational space:**
 * Exciting but not fully realised

**Liz's Creatures:**
 * Courses for creating creatures
 * Like a Ferby but open source
 * Connecting creatures, across the world

**Tours of Wellington:**
 * A touring system for Wellington
 * Op shop tours, Sustainable Business tour, Linking it in with different local businesses. (Look on Meetup)
 * Connecting different tours
 * Uber type system
 * App design, training system


 **Herding Lights:**
 * Turning one light on, turns others on
