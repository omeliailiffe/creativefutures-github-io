---
layout: onepage
title: "Overview"
---
We are going to be designing and producing an artefact capable of emoting. The artefact lives within the home and their visual, and audio output is informed by the environment around them (that might be temperature, sound presence of humans, or something else). Each artefact is connected, with a herd-like mentality, to other artefacts in the area, forming an anonymous community, with each artefacts influencing how the herd is emoting. This artefact will either be marketed as a DIY kit, a workshop, or a more traditional product.


### Our Team
##### Harry (Industrial Design)
Specialising in electronic design, system design, web design, and digital fabrication.
##### Liz (Industrial Design)
Focusing on form design, materiality, digital fabrication, sculpture.
##### Greer (Visual Communication)
Specialising in typography, illustration and multimedia design.
##### Rj (Visual Communication)
Specialising in graphic design, spatial, AR/VR Technology and web/app design.

##### We will design:
 * An Emotive Artefact (Form Design, Electronic Design, Interaction Design )
 * A System of Artefacts (System Design)
 * A Website (Visual Communication Design, Full Stack Developer)
